import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule }   from '@angular/forms';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    AgmCoreModule.forRoot({
		apiKey: 'AIzaSyB7bcE-Muh4S5YfnhrPwOjQckzzlLBtuUo',
		libraries: ["places"]
	})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }