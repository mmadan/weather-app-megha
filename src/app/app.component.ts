import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { MapsAPILoader } from '@agm/core';
import {} from '@types/googlemaps';
import { ViewChild, ElementRef, NgZone } from '@angular/core';
import Chart from 'chart.js';


export class Loc {
	latitude: number;
	longitude: number;
}

export class Result {
	summary: string;
	location: string;
	show: boolean;
	currentTemp: number;
	future: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'my Weather App';

  result: Result = {
  	summary: ' ',
  	location: ' ',
  	show: false,
  	currentTemp: null,
  	future: ' '
  };

  loc: Loc = {
  	latitude: null,
  	longitude: null
  };

  @ViewChild('search') public searchElement: ElementRef;
  @ViewChild('myChart') Chart: ElementRef;

  constructor(private http: Http, private mapsAPILoader: MapsAPILoader, private ngZone: NgZone) {}


   	ngOnInit() {
		this.mapsAPILoader.load().then(() => {
		    let autocomplete = new google.maps.places.Autocomplete(this.searchElement.nativeElement, {} );
			autocomplete.addListener("place_changed", () => {
				this.ngZone.run(() => {
					let place: google.maps.places.PlaceResult = autocomplete.getPlace();
					this.loc.latitude = place.geometry.location.lat();
					this.loc.longitude = place.geometry.location.lng();
					this.result.location = place.formatted_address;

					if(place.geometry === undefined || place.geometry === null ){
			            return;
			         }
		        });
		    });
	    });
    }

  findWeather(loc: Loc): void {
  	if(loc.latitude != null && loc.longitude != null){
		this.http.get('https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/f8b2873aa9ef716dd859bf3640a1a03e/' +  loc.latitude + ',' + loc.longitude).subscribe(data => {
		    var obj = data.json();
		    this.result.summary = obj.currently.summary;
		    this.result.currentTemp = obj.currently.temperature;
		    this.result.future = obj.daily.summary;

		   	var ctx = this.Chart.nativeElement.getContext('2d');
			var g = new Chart(ctx, {
	    	type: 'bar',
	    	data: {
	        	labels: ["Today", "Today+1", "Today+2", "Today+3", "Today+4", "Today+5"],
	        	datasets: [{
		            label: 'Highest Temperatures Next 5 Days in Farenheit',
		            data: [
		            		obj.daily.data[0].temperatureHigh, 
		            		obj.daily.data[1].temperatureHigh, 
		            		obj.daily.data[2].temperatureHigh, 
		            		obj.daily.data[3].temperatureHigh, 
		            		obj.daily.data[4].temperatureHigh, 
		            		obj.daily.data[5].temperatureHigh
		            ],
		            backgroundColor: 'rgba(54, 162, 235, 0.2)',
		            borderColor: 'rgba(54, 162, 235, 1)',
		            borderWidth: 1
	        	}]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        }
		    }
			});
		});
	}
  }

  refreshGrid(): void {
  	window.location.reload();
  }
}
